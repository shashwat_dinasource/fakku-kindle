header() {
	echo "<html>
	<head>
	<meta name="author" content="$bookname" />
	<meta name="cover" content=\"$(ls|grep '.png\|.jpg'|head -n 1)\" />
	<title>$bookname</title>
	</head>
	<body>
	">$bookname.html
}

images() {
  for f in $(ls | grep '.png\|.jpg')
  do
    echo "<p><img src=\"$f\" /></p>">>$bookname.html
    # do something useful
  done
}


footer() {
	echo "</body>
	</html>">>$bookname.html
}

mobigenerate() {
	./kindlegen $bookname.html
}

moveFileUpdateStatus() {
	mv $bookname.mobi $OPENSHIFT_REPO_DIR/php/books/$bookname.mobi
}

#echo $bookname"Says main"
