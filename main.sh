export ul=$1
export mangaurl="$ul/download"
export bookname=$(echo $ul|awk 'BEGIN { FS = "/" } ; { print $5 }')}

main() {
	downFile
	extractFile
	header
	images
	footer
	mobigenerate
	cleanUp
	moveFileUpdateStatus
}

#echo $ul
#echo $bookname
#echo $mangaurl
cd $OPENSHIFT_DATA_DIR/images
source bookconvert.sh
source down.sh
main
